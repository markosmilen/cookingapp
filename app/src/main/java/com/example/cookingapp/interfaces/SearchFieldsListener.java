package com.example.cookingapp.interfaces;

import android.view.View;

public interface SearchFieldsListener {

    void onSearchButtonClicked(View view);
}
